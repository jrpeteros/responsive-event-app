import React from "react";
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Toolbar, SidePanel, Backdrop, CreateForm, ViewForm }  from './components';
import axios from 'axios';

class App extends React.Component{
    state = {
        sidePanelOpen: false,
        isLoading: false,
        error: ''
    };

    backdropClickHandler = () => {
        this.setState({sidePanelOpen: false});
    }

    handleSubmit =  async (type, serviceId, icon, title, data) => {
        this.setState({isLoading: true});
        const dt = new Date();
        const utcDate = dt.toUTCString();

        const resp = await axios({
            method: 'post',
            url: 'https://forgetful-elephant.herokuapp.com/events',
            data: {
                type: type,
                serviceId: serviceId,
                icon: icon,
                timestamp: utcDate,
                title: title,
                data: data
            }
        })

        if(resp){
            this.setState({
                sidePanelOpen: true,
                isLoading: false
            });
        } else {
            this.setState({ error: resp});
        }
        
    }

    render() {
        const { isLoading, sidePanelOpen } = this.state;
        let backdrop;
        let loader;

        if(sidePanelOpen) {
            backdrop = <Backdrop click={this.backdropClickHandler}/>;
        }
        if(isLoading) {
            loader = <div className="loader"> </div>;
        }
        return(
            <Router>
                <div className="App">
                { loader }
                <Toolbar/>
                <SidePanel show={this.state.sidePanelOpen}/>
                { backdrop }
                <Route
                    exact strict 
                    path='/'
                    render={() => 
                        <CreateForm onFormSubmit={this.handleSubmit}/>
                    }
                />
                <Route path="/view" exact strict component={ViewForm} />
            </div>
            </Router>
        ) 
    }
}

export default App;