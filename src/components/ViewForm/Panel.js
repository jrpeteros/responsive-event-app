import React, { useState, useRef } from "react";

import "./Panel.scss";

const Panel = ({ id, type, serviceId, icon, title, timestamp, data }) => {
    const [setActive, setActiveState] = useState("");
    const [setHeight, setHeightState] = useState("0px");

    const content = useRef(null);

    function toggleAccordion() {
        setActiveState(setActive === "" ? "active" : "");
        setHeightState(
        setActive === "active" ? "0px" : `${content.current.scrollHeight}px`
        );
    }

    let imageVar;
    if(icon) {
        imageVar = <img src={icon} alt="icon"/>;
    } else {
        imageVar = <i className="fa fa-bullhorn"> </i>
    }

  return (
    <div className="accordion__section">
      <button className={`accordion ${setActive}`} onClick={toggleAccordion}>
        <p className="accordion__title"> {imageVar} {title} </p>
        <p className="accordion__title">(event) {type}</p>
      </button>
      <div
        ref={content}
        style={{ maxHeight: `${setHeight}` }}
        className="accordion__content"
      >
        <div className="accordion__text">
            <p> {imageVar} </p>
            <p> <b>Title:</b> {title} </p>
            <p> <b> Event: </b> {title} </p>
            <p> <b>Service Id: </b>{serviceId} </p>
            <p> <b>Date/Time published:</b> {timestamp} </p>
            <p> {data} </p>
        </div>
      </div>
    </div>
  );
}

export default Panel;
