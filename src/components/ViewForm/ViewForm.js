import React from 'react';
import axios from 'axios';
import './ViewForm.scss';
import Panel from "./Panel";

class ViewForm extends React.Component {

    constructor(props) {
        // Pass props to parent class
        super(props);
        // Set initial state
        this.state = {
            events: [],
            isLoading: true,
            searchEvent: '',
            error: ''
        }
        this.apiUrl = 'https://forgetful-elephant.herokuapp.com/events'
    }

    componentDidMount() {
        this.getEvents();
    }

    getEvents() {
        axios.get(this.apiUrl)
            .then(response =>
                response.data.map(event => ({
                    id: `${event.id}`,
                    type: `${event.type}`,
                    serviceId: `${event.serviceId}`,
                    icon: `${event.icon}`,
                    timestamp: `${event.timestamp}`,
                    title: `${event.title}`,
                    data: `${event.data}`
                }))
            )
            .then(events => {
                this.setState({
                    events,
                    isLoading: false
                });
            })
            .catch(error => this.setState({ error: error, isLoading: false }));
    }

    handleDelete = (id) => {
        // Filter all events except the one to be removed
        let filterEvent = this.state.events.filter((event) => {
            return (event.id !== id ? event : null);
        });
        this.setState({ isLoading: true });
        // Update state with filter
        axios.delete(this.apiUrl + '/' + id)
            .then((res) => {
                this.setState({ events: filterEvent, isLoading: false });
            })
    }

    handleChange = (event) => {
        this.setState({ searchEvent: event.target.value })
    }

    render() {
        const { isLoading, events, searchEvent } = this.state;
        let filterEvents = events.filter((event) => {
            return event.type.toLowerCase().includes(searchEvent.toLowerCase())
        });
        return (
            <React.Fragment>
                <div className="filterBox">
                { !isLoading || !filterEvents || !events? (
                    <input type="text" id="filter" 
                        placeholder="Start searching an event e.g Sports, Technology, Science"
                        onChange={this.handleChange}
                        />
                ) : null
                }
                </div>
                <div className={isLoading ? "": "container"}>
                    {
                        !isLoading ? (
                            filterEvents.map(event => {
                                const { id, type, serviceId, timestamp, icon, title, data } = event;
                                return (
                                    <div key={id}>
                                        <div className='alignRight'>
                                        <div className='alignRight__circle' onClick={() => this.handleDelete(id)}>
                                            <p className='alignRight__text-white'>X</p>
                                        </div>
                                        </div>
                                        <Panel
                                            id = {id}
                                            type = {type}
                                            timestamp = {timestamp}
                                            serviceId = {serviceId}
                                            icon = {icon} 
                                            title = {title} 
                                            data = {data}
                                        />
                                    </div>
                                );
                            })
                        ) : <div className="loader"> </div>
                    }
                </div>
            </React.Fragment>
        );
    }
}
export default ViewForm;