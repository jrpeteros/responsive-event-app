import React from 'react';

import './CreateForm.scss';

class CreateForm extends React.Component {

    state = {
        type: '',
        serviceId: '',
        icon: '', 
        title: '',
        data: ''
    }

    handleChange = (event) => {
        let refState = {};
        refState[event.target.name] = event.target.value
        this.setState(refState);
    }

    handleSubmit = (event, props) => {
        const { type, serviceId, icon, title, data } = this.state;
        const { onFormSubmit } = this.props;

        onFormSubmit(type, serviceId, icon, title, data);
        event.preventDefault();
        
    }

    render() {
        return (
            <div>
                <div className="panel-logo">
                    <i className="fa fa-bullhorn"> 
                        <span> Event Center </span>  
                    </i>
                </div>
                <div className="container">
                    <form onSubmit={this.handleSubmit}>
                        <div className="form-row">
                            <div className="form-row__title" htmlFor="event">Event Type</div>
                            <div className="form-row__content">
                                <div className="form-row__content-left">
                                    <input type="text" name="type" onChange={this.handleChange}/>
                                </div>
                                <div className="form-row__content-right"> Define a general event. e.g "Sports", "Technology"</div>
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="form-row__title" htmlFor="type">Service Id</div>
                            <div className="form-row__content">
                                <div className="form-row__content-left">
                                    <input type="text" maxLength="50"  name="serviceId" onChange={this.handleChange}/>
                                </div>
                                <div className="form-row__content-right"> Provide the service id.</div>
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="form-row__title" htmlFor="event">Title</div>
                            <div className="form-row__content">
                                <div className="form-row__content-left">
                                    <input type="text" maxLength="100" name="title" onChange={this.handleChange}/>
                                </div>
                                <div className="form-row__content-right"> Provide brief title summary of your event.</div>
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="form-row__title" htmlFor="event">URL icon</div>
                            <div className="form-row__content">
                                <div className="form-row__content-left">
                                    <input type="text" name="icon" onChange={this.handleChange}/>
                                </div>
                                <div className="form-row__content-right"> url icon for this event.</div>
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="form-row__title" htmlFor="dataSummary">Summary</div>
                            <div className="form-row__content">
                                <div className="form-row__content-left">
                                    <textarea id="data" name="data" onChange={this.handleChange}/>
                                </div>
                                <div className="form-row__content-right"> What we really need is to understand what your event is. What trigger the event e.g "I tried to jump over the lazy fox."</div>
                            </div>
                        </div>
                        <div className="form-row">
                            <input type="submit" value="Submit" onClick={this.handleSubmit}/>
                            <a  className="form-row__cancel-btn"href="/"> Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}


export default CreateForm;