import React from 'react';

import './SidePanel.scss';

const SidePanel = (props) => {
    let sidePanelClass = ['side-panel'];
    if(props.show){
        sidePanelClass = ['side-panel open'];
    }
    return (
        <nav className={sidePanelClass}>
            <ul>
                <li> <a href="/"> <i className="fa fa-check"></i>Created Successfully</a></li>
            </ul>
        </nav>
    )
}

export default SidePanel;