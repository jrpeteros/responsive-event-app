import React from 'react'
import './Toolbar.scss'

const Toolbar = (props) => (
  <header className="toolbar">
    <nav className="toolbar__navigation">
      <div className="toolbar__logo">
        <a href="/">THE EVENT DIRECTORY</a>
      </div>
      <div className="spacer" />
      <div className="toolbar__navigation-items">
        <ul>
          <li>
            <a href="/">Create</a>
          </li>
          |
          <li>
            <a href="/view">View</a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
)

export default Toolbar;