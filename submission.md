#  Responsive Event App

Welcome to simple Responsive Event App built on ReactJS.
No other frameworks needed.

## Summary
A web-service application capable of storing, fetching and deleting events emitted by a variety of clients.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development purposes. 

### Prerequisites

**Knowledge Prerequisites:**

1. Knowledge of `HTML`, `CSS`
2. Knowledge of  [`SCSS/LESS/SASS`](https://sass-lang.com/)
3. Knowledge of `Javascript` / [`ES6`](https://www.w3schools.com/js/js_es6.asp)
4. Knowledge of [`ReactJS`](https://www.w3schools.com/react/)
5. Knowledge of `RESTful API` and [`Axios`](https://github.com/axios/axios)

**Software Installed Prerequisites:**

1. [Yarn](https://yarnpkg.com/lang/en/docs/cli/global) Latest version
2. [NodeJs](https://nodejs.org/en/) Latest Version

**Optional installation:**

1. [Create-react-app](https://create-react-app.dev/docs/getting-started)


### Installing 
Please follow the steps below **IN ORDER**:

1. Clone this repo `git clone https://jrpeteros@bitbucket.org/jrpeteros/responsive-event-app.git`
2. Ensure that you are at the project root in your terminal
3. Run `yarn` in your terminal & wait until you will see something below. 

```
warning " > babel-loader@8.0.6" has unmet peer dependency "@babel/core@^7.0.0".
warning "react-scripts > @typescript-eslint/eslint-plugin > tsutils@3.17.1" has unmet peer dependency "typescript@>=2.8.0 || >= 3.2.0-dev || >= 3.3.0-dev || >= 3.4.0-dev || >= 3.5.0-dev || >= 3.6.0-dev || >= 3.6.0-beta || >= 3.7.0-dev || >= 3.7.0-beta".
[4/4] 🔨  Building fresh packages...
success Saved lockfile.
✨  Done in 52.23s.
```

### Running the app
**Prerequisite:** Installing steps above have to be completed correctly

1. Run `yarn start` in your terminal & wait until you will see something below. 

```
Compiled successfully!
You can now view responsive-event-app in the browser.
  Local:            http://localhost:3000/
  On Your Network:  http://192.168.0.126:3000/
```

2. Now go to a browser and visit this url `http://localhost:3000/`


## Error encounter on installation
**If you encounter some error shown below**
```
./src/index.scss (./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-5-1!./node_modules/postcss-loader/src??postcss!./node_modules/resolve-url-loader??ref--6-oneOf-5-3!./node_modules/sass-loader/lib/loader.js??ref--6-oneOf-5-4!./src/index.scss)

Run `npm install node-sass` or `yarn add node-sass` inside your workspace.
```
Please follow steps: 

1. Run `yarn add node-sass`
2. Run `yarn start` again

**If you encounter some error shown below**
```
return (new fsevents(path)).on('fsevent', callback).start();
```
Please follow steps: 

1. Remove `node_momdules` folder and `yarn.lock` file
2. Run `yarn start` again


## Misc
*  [DEMO](https://responsive-event-app.herokuapp.com/)

### And coding style tests
* <NA>

## Running the tests
* <NA>

### Break down into end to end tests
* <NA>

## Deployment
* <NA>

## Author
* Jose Ricardo Peteros
